package ai.sparklabinc.woos;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.history.HistoricTaskInstance;
import org.activiti.engine.repository.DeploymentBuilder;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.junit.Test;

import ai.sparklabinc.woos.util.PropertiesUtil;

public class test {
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	// @Test
	public void startProcessInstance() {
		// 流程定义的key
		String processDefinitionKey = "PO_002";
		ProcessInstance pi = processEngine.getRuntimeService()// 与正在执行的流程实例和执行对象相关的Service
				.startProcessInstanceByKey(processDefinitionKey);// 使用流程定义的key启动流程实例，key对应helloworld.bpmn文件中id的属性值，使用key值启动，默认是按照最新版本的流程定义启动
		System.out.println("流程实例ID:" + pi.getId());// 流程实例ID
		System.out.println("流程定义ID:" + pi.getProcessDefinitionId());// 流程定义ID
	}

	/** 查询当前人的个人任务 */
	// @Test
	public void findMyPersonalTask() {
		String assignee = "郭靖";
		List<Task> list = processEngine.getTaskService()// 与正在执行的任务管理相关的Service
				.createTaskQuery()// 创建任务查询对象
				/** 查询条件（where部分） */
				.taskAssignee(assignee)// 指定个人任务查询，指定办理人
				// .taskCandidateUser(candidateUser)//组任务的办理人查询
				/** 排序 */
				.orderByTaskCreateTime().asc()// 使用创建时间的升序排列
				/** 返回结果集 */
				.list();// 返回列表
		if (list != null && list.size() > 0) {
			for (Task task : list) {
				System.out.println("任务ID:" + task.getId());
				System.out.println("任务名称:" + task.getName());
				System.out.println("任务的创建时间:" + task.getCreateTime());
				System.out.println("任务的办理人:" + task.getAssignee());
				System.out.println("流程实例ID：" + task.getProcessInstanceId());
				System.out.println("执行对象ID:" + task.getExecutionId());
				System.out.println("流程定义ID:" + task.getProcessDefinitionId());
				System.out.println("########################################################");
			}
		}
	}

	/** 查询当前人的组任务 */
	// @Test
	public void findMyGroupTask() {
		// String candidateUser = "郭靖";
		String candidateUser = "黄蓉";
		List<Task> list = processEngine.getTaskService()// 与正在执行的任务管理相关的Service
				.createTaskQuery()// 创建任务查询对象
				/** 查询条件（where部分） */
				.taskCandidateUser(candidateUser)// 组任务的办理人查询
				/** 排序 */
				.orderByTaskCreateTime().asc()// 使用创建时间的升序排列
				/** 返回结果集 */
				.list();// 返回列表
		if (list != null && list.size() > 0) {
			for (Task task : list) {
				System.out.println("任务ID:" + task.getId());
				System.out.println("任务名称:" + task.getName());
				System.out.println("任务的创建时间:" + task.getCreateTime());
				System.out.println("任务的办理人:" + task.getAssignee());
				System.out.println("流程实例ID：" + task.getProcessInstanceId());
				System.out.println("执行对象ID:" + task.getExecutionId());
				System.out.println("流程定义ID:" + task.getProcessDefinitionId());
				System.out.println("########################################################");
			}
		}
	}

	private PropertiesUtil propertiesUtil = new PropertiesUtil("application.properties");

	@Test
	public void testest() throws UnknownHostException {
		/*
		 * DeploymentBuilder MD_001Builder =
		 * processEngine.getRepositoryService().createDeployment();// 创建一个部署对象
		 * MD_001Builder.name("marven");// 添加部署的名称
		 * MD_001Builder.addClasspathResource("static/diagrams/marven_test_reject.bpmn")
		 * ;// 从classpath的资源加载，一次只能加载一个文件
		 * MD_001Builder.addClasspathResource("static/diagrams/marven_test_reject.png");
		 * // 从classpath的资源加载，一次只能加载一个文件 MD_001Builder.deploy();
		 */
		// 完成部署

		/*
		 * RuntimeService runtimeService = processEngine.getRuntimeService(); //
		 * 使用流程定义的key，key对应bpmn文件对应的id， String processDefinitionkey = "marven";//
		 * 流程定义的key就是HelloWorld // 获取流程实例对象 ProcessInstance processInstance =
		 * runtimeService.startProcessInstanceByKey(processDefinitionkey);
		 */
		// processEngine.getTaskService().complete("604");
		System.out.println(InetAddress.getLocalHost().getHostAddress());
		System.out.println(propertiesUtil.getValue("server.port"));
	}

}
