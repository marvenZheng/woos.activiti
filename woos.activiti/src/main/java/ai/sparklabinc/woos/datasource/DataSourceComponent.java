package ai.sparklabinc.woos.datasource;

import java.sql.Connection;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import ai.sparklabinc.woos.commons.PropertyConfigure;

@Component
public class DataSourceComponent implements InitializingBean {

	private DataSource identityDataSource = new DataSource();

	@Override
	public void afterPropertiesSet() throws Exception {
		String[] propertiesArray = { "jdbc.mysql.identity.properties" };
		for (String properties : propertiesArray) {
			if (!PropertyConfigure.getInstance().load(properties)) {

			}
		}
		identityDataSource.init("jdbc.mysql.identity");
	}

	public DataSource getIdentityDataSource() {
		return identityDataSource;
	}

	public Connection getConn() {
		DataSource dataSource = this.getIdentityDataSource();
		if (dataSource == null) {
			identityDataSource.init("jdbc.mysql.identity");
		}
		return identityDataSource.getConnection();
	}

}
