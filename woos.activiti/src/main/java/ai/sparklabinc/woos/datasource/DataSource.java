package ai.sparklabinc.woos.datasource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import ai.sparklabinc.woos.commons.PropertyConfigure;

public class DataSource {
	private static final Logger LOGGER = Logger.getLogger(DataSource.class);

	private ComboPooledDataSource mPooledDataSource;

	public boolean init(String propertiesPrefix) {
		mPooledDataSource = new ComboPooledDataSource();

		// JDBC Driver Class
		String jdbcDriverClass = PropertyConfigure.getInstance().getStringProperty(propertiesPrefix + ".driverClass",
				null);
		if (jdbcDriverClass == null) {
			LOGGER.fatal("Failed to load JDBC dirver.");
			return false;
		}

		try {
			mPooledDataSource.setDriverClass(jdbcDriverClass);
		} catch (PropertyVetoException e) {
			LOGGER.fatal("Faild to set JDBC driver class " + jdbcDriverClass, e);
			return false;
		}

		// JDBC URL
		String jdbcUrl = PropertyConfigure.getInstance().getStringProperty(propertiesPrefix + ".jdbcUrl", null);
		if (jdbcUrl == null) {
			LOGGER.fatal("Failed to load JDBC url.");
			return false;
		}
		mPooledDataSource.setJdbcUrl(jdbcUrl);

		// JDBC User
		String user = PropertyConfigure.getInstance().getStringProperty(propertiesPrefix + ".user", null);
		if (user == null) {
			LOGGER.fatal("Failed to load JDBC user.");
			return false;
		}
		mPooledDataSource.setUser(user);

		// JDBC Password
		String password = PropertyConfigure.getInstance().getStringProperty(propertiesPrefix + ".password", null);
		if (password == null) {
			LOGGER.fatal("Failed to load JDBC password.");
			return false;
		}
		mPooledDataSource.setPassword(password);

		// More Configurations
		try {
			mPooledDataSource.setInitialPoolSize(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".initialPoolSize", 2));

			mPooledDataSource.setMaxPoolSize(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".maxPoolSize", 3));

			mPooledDataSource.setMinPoolSize(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".minPoolSize", 1));

			mPooledDataSource.setAcquireIncrement(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".acquireIncrement", 10));

			mPooledDataSource.setIdleConnectionTestPeriod(PropertyConfigure.getInstance()
					.getIntegerProperty(propertiesPrefix + ".idleConnectionTestPeriod", 60));

			mPooledDataSource.setMaxIdleTime(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".maxIdleTime", 3600));

			mPooledDataSource.setAutoCommitOnClose(
					PropertyConfigure.getInstance().getBooleanProperty(propertiesPrefix + ".autoCommitOnClose", true));

			mPooledDataSource.setAcquireRetryAttempts(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".acquireRetryAttempts", 30));

			mPooledDataSource.setAcquireRetryDelay(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".acquireRetryDelay", 1000));

			mPooledDataSource.setBreakAfterAcquireFailure(PropertyConfigure.getInstance()
					.getBooleanProperty(propertiesPrefix + ".breakAfterAcquireFailure", false));

			mPooledDataSource.setCheckoutTimeout(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".checkoutTimeout", 30000));

			mPooledDataSource.setNumHelperThreads(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".numHelperThreads", 3));

			mPooledDataSource.setLoginTimeout(
					PropertyConfigure.getInstance().getIntegerProperty(propertiesPrefix + ".loginTimeout", 60));

		} catch (Exception e) {
			LOGGER.fatal("Failed to set JDBC parameters.", e);
			return false;
		}

		return true;
	}

	public Connection getConnection() {
		try {
			return mPooledDataSource.getConnection();
		} catch (SQLException e) {
			LOGGER.error("Failed to get a JDBC connection.", e);
		}

		return null;
	}

}
