package ai.sparklabinc.woos.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import ai.sparklabinc.woos.entity.DataIdAndActivitiRelations;

@Repository
public interface DataIdAndActivitiRelationsRepository extends CrudRepository<DataIdAndActivitiRelations, Long> {

	@Query("select u from DataIdAndActivitiRelations u where u.dataId =:dataId and u.modelId=:modelId")
	DataIdAndActivitiRelations findByDataId(@Param("dataId") String dataId, @Param("modelId") String modelId);

	@Query("select u from DataIdAndActivitiRelations u where u.processInstaceId=:processInstaceId")
	DataIdAndActivitiRelations findByProcessInstanceId(@Param("processInstaceId") String processInstaceId);
}
