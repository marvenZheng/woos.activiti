package ai.sparklabinc.woos.api;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import ai.sparklabinc.woos.activiti.service.ActivitiService;
import ai.sparklabinc.woos.commons.activiti.ActivitiCommand;
import ai.sparklabinc.woos.commons.activiti.ActivitiParamsBean;

/**
 * activiti controller
 * 
 * @author marven
 *
 */
@RestController
@RequestMapping(value = "/activiti")
public class ActivitiApiController {

	@Autowired
	private ActivitiService activitiService;

	@RequestMapping(value = "/createBatch", method = RequestMethod.POST)
	@ResponseBody
	public Object createBatchActiviti(@RequestBody List<ActivitiCommand> activitiCommands, HttpServletRequest request)
			throws Exception {
		return this.activitiService.createBatch(activitiCommands);
	}

	@RequestMapping(value = "/query", method = RequestMethod.GET)
	@ResponseBody
	public Object queryDetail(String dataId, String moduleId, HttpServletRequest request) throws Exception {

		return null;
	}

	@RequestMapping(value = "/approve", method = RequestMethod.POST)
	@ResponseBody
	public Object approveActiviti(@RequestBody List<ActivitiParamsBean> activitiParamsBean, HttpServletRequest request)
			throws Exception {
		return this.activitiService.approveProcess(activitiParamsBean);
	}

	@RequestMapping(value = "/reject", method = RequestMethod.POST)
	@ResponseBody
	public Object rejectActiviti(@RequestBody List<ActivitiParamsBean> activitiParamsBean, HttpServletRequest request)
			throws Exception {
		return this.activitiService.rejectProcess(activitiParamsBean);
	}

	@RequestMapping(value = "/close", method = RequestMethod.POST)
	@ResponseBody
	public Object closeActiviti(@RequestBody List<ActivitiParamsBean> activitiParamsBean, HttpServletRequest request)
			throws Exception {
		return this.activitiService.closeProcess(activitiParamsBean);
	}

	@RequestMapping(value = "/claim", method = RequestMethod.POST)
	@ResponseBody
	public Object claim(String processInstanceId, String fromUserName, String toUserName, HttpServletRequest request)
			throws Exception {
		return this.activitiService.claimProcess(processInstanceId, fromUserName, toUserName);
	}

}
