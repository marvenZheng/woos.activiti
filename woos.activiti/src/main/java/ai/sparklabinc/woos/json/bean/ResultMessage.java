package ai.sparklabinc.woos.json.bean;

/**
 * 统一的bean，用来返回信息到前台 可以为新增，修改，删除等请求服务
 * 
 * @author marven
 *
 */
public class ResultMessage {

	/**
	 * 状态
	 */
	private boolean status;

	/**
	 * 信息
	 */
	private String message;

	/**
	 * 返回数据
	 */
	private String resultData;

	public ResultMessage(boolean status, String message, String resultData) {
		super();
		this.status = status;
		this.message = message;
		this.resultData = resultData;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getResultData() {
		return resultData;
	}

	public void setResultData(String resultData) {
		this.resultData = resultData;
	}

}
