package ai.sparklabinc.woos.exception;

public class DuplicateDataException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1099426243558252565L;

	public DuplicateDataException(String message) {
		super(message);
	}
}
