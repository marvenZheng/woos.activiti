package ai.sparklabinc.woos.exception;

public class ParamsIllegalException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1099426243558252565L;

	public ParamsIllegalException(String message) {
		super(message);
	}
}
