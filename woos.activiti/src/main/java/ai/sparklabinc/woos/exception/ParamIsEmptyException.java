package ai.sparklabinc.woos.exception;

public class ParamIsEmptyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5373123770046929684L;

	public ParamIsEmptyException(String message) {
		super(message);
	}
}
