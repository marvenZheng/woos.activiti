package ai.sparklabinc.woos.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * actionCode与角色的对应关系
 * 
 * @author marven
 *
 */
public class Constants {

	public static Map<String, String> actionCodeAndRoles = new HashMap<String, String>();
	static {
		actionCodeAndRoles.put("PO_002", "DSR");
		actionCodeAndRoles.put("PO_001", "AEA,DSR");
		actionCodeAndRoles.put("MD_002", "AEA");
		actionCodeAndRoles.put("PS_001", "CTLL");
		actionCodeAndRoles.put("MD_001", "AEA");

	}

	/**
	 * all workflow actionCodes
	 * 
	 * @author marven
	 *
	 */
	public static enum ACTIONCODES {
		Close_001, MD_001, MD_002, PO_001, PO_002, PS_001
	}
}
