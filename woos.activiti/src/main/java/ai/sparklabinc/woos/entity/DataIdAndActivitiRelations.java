package ai.sparklabinc.woos.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * dataid 对应的activiti 关系
 * 
 * 一个dataid对应一个activiti工作流
 * 
 * @author marven
 *
 */
@Entity
@Table(name = "act_woos_relations")
public class DataIdAndActivitiRelations implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5499175581137244839L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	/**
	 * dataid对应oosreport的每条数据，唯一的uuid+moduleId
	 */
	@Column(name = "data_id")
	private String dataId;

	/**
	 * activiti 库中对应唯一的processInstaceId
	 */
	@Column(name = "process_instance_id")
	private String processInstaceId;

	/**
	 * 次工作流对应的图片路径
	 */
	@Column(name = "activiti_photo_path")
	private String activitiPhotoPath;

	/**
	 * 当前数据对应的处理状态，处理中:In_Process，驳回：Rejected，完成：Completed，关闭：Closed
	 */
	@Column(name = "status")
	private Status status;

	@Column(name = "model_id")
	private String modelId;

	@Column(name = "current_handler")
	private String currentHandler;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDataId() {
		return dataId;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	public String getProcessInstaceId() {
		return processInstaceId;
	}

	public void setProcessInstaceId(String processInstaceId) {
		this.processInstaceId = processInstaceId;
	}

	public String getActivitiPhotoPath() {
		return activitiPhotoPath;
	}

	public void setActivitiPhotoPath(String activitiPhotoPath) {
		this.activitiPhotoPath = activitiPhotoPath;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public String getModelId() {
		return modelId;
	}

	public void setModelId(String modelId) {
		this.modelId = modelId;
	}

	public String getCurrentHandler() {
		return currentHandler;
	}

	public void setCurrentHandler(String currentHandler) {
		this.currentHandler = currentHandler;
	}

	// 处理中:In_Process，驳回：Rejected，完成：Completed，关闭：Closed,挂起：Suspend
	public static enum Status {
		IN_PROCESS, CLOSED, COMPLETED, INITIAL, SUSPEND, INVALID, REJECTED, CANCEL;
		;
	}

}
