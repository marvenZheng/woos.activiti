package ai.sparklabinc.woos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableEurekaClient
@ComponentScan(basePackages = { "ai.sparklabinc.woos" })
public class App {
    public static void main(String[] args) {
        SpringApplication.run(App.class, args);
    }
}
