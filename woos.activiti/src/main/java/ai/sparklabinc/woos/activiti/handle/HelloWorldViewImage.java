package ai.sparklabinc.woos.activiti.handle;

import java.io.File;
import java.io.InputStream;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.repository.Deployment;
import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class HelloWorldViewImage {
	// 获取流程引擎对象
	// getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/** 查看流程附件(查看流程图片) */
	@Test
	public void viewImage() throws Exception {
		// 从仓库中找需要展示的文件
		String deploymentId = "1";
		List<String> names = processEngine.getRepositoryService().getDeploymentResourceNames(deploymentId);
		/*List<Deployment> dpDatas = processEngine.getRepositoryService().createDeploymentQuery().list();
		for (Deployment deployment : dpDatas) {
			String iName = deployment.getName();
			File f = new File(iName);
			// 通过部署ID和文件名称得到文件的输入流
			InputStream in = processEngine.getRepositoryService().getResourceAsStream(deploymentId, iName);
			FileUtils.copyInputStreamToFile(in, f);
		}*/
		String imageName = null;
		for (String name : names) {
			System.out.println("name:" + name);
			if (name.indexOf(".png") > 0) {
				imageName = name;
				break;
			}
		}
		System.out.println("imageName:" + imageName);
		if (imageName != null) {
			File f = new File("diagrams/hhhh.png");
			// 通过部署ID和文件名称得到文件的输入流
			InputStream in = processEngine.getRepositoryService().getResourceAsStream(deploymentId, imageName);
			FileUtils.copyInputStreamToFile(in, f);
		}
	}
}
