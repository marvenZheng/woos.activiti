package ai.sparklabinc.woos.activiti.listener;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.activiti.engine.delegate.DelegateTask;
import org.activiti.engine.delegate.TaskListener;

import ai.sparklabinc.woos.util.JDBCUtil;

public class TaskPO002ListenerImpl implements TaskListener {
	Connection conn = JDBCUtil.getConnection();

	/** 用来指定任务的办理人 */
	@Override
	public void notify(DelegateTask delegateTask) {
		List<String> usernames = new ArrayList<String>();
		try {
			ResultSet rs = conn.prepareStatement("select id,name from id_group where name='DSR';").executeQuery();
			while (rs.next()) {
				usernames.add(rs.getString(1) + "," + rs.getString(2));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// 指定个人任务的办理人，也可以指定组任务的办理人
		// 个人任务：通过类去查询数据库，将下一个任务的办理人查询获取，然后通过setAssignee()的方法指定任务的办理人
		// delegateTask.setAssignee("灭绝师太");
		// 组任务：
		for (String string : usernames) {
			delegateTask.addCandidateUser(string);
		}
	}
}