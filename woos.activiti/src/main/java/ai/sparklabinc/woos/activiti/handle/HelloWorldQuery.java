package ai.sparklabinc.woos.activiti.handle;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.activiti.engine.repository.Model;
import org.activiti.engine.repository.ProcessDefinition;
import org.activiti.engine.runtime.Execution;
import org.activiti.engine.task.Task;
import org.junit.Test;

public class HelloWorldQuery {
	// 获取流程引擎对象
	// getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	// @Test
	public void findMyPersonalTask() {
		List<Model> models = processEngine.getRepositoryService().createModelQuery().list();
		for (Model model : models) {
			System.out.println(model.getName());
		}
		String assignee = "marven";
		// 获取事务Service
		Map<String, Object> map = processEngine.getRuntimeService().getVariables(assignee);
		for (Entry<String, Object> m : map.entrySet()) {
			System.out.println(m.getValue() + "----" + m.getKey());
		}
		List<Execution> datas = processEngine.getRuntimeService().createExecutionQuery().variableValueEquals(assignee)
				.list();
		System.out.println(datas.size() + "--------------------");
		TaskService taskService = processEngine.getTaskService();
		List<Task> taskList = taskService.createTaskQuery()// 创建任务查询对象
				.taskAssignee(assignee)// 指定个人任务查询，指定办理人
				.list();// 获取该办理人下的事务列表

		if (taskList != null && taskList.size() > 0) {
			for (Task task : taskList) {
				System.out.println("任务ID：" + task.getId());
				System.out.println("任务名称：" + task.getName());
				System.out.println("任务的创建时间：" + task.getCreateTime());
				System.out.println("任务办理人：" + task.getAssignee());
				System.out.println("流程实例ID：" + task.getProcessInstanceId());
				System.out.println("执行对象ID：" + task.getExecutionId());
				System.out.println("流程定义ID：" + task.getProcessDefinitionId());
				System.out.println("#############################################");
			}
		}
	}

	/**
	 * 查看流程定义 id:(key):(version):(随机值) name:对应流程文件process节点的name属性
	 * key:对应流程文件process节点的id属性 version:发布时自动生成的。如果是第一次发布的流程，version默认从1开始；
	 * 如果当前流程引擎中已存在相同的流程，则找到当前key对应的最高版本号，在最高版本号上加1
	 */
	// @Test
	public void queryProcessDefinition() throws Exception {
		// 获取仓库服务对象，使用版本的升级排列，查询列表
		List<ProcessDefinition> pdList = processEngine.getRepositoryService().createProcessDefinitionQuery()
				// 添加查询条件
				// .processDefinitionId(processDefinitionId)
				// .processDefinitionKey(processDefinitionKey)
				// .processDefinitionName(processDefinitionName)
				// 排序(可以按照id/key/name/version/Cagetory排序)
				.orderByProcessDefinitionVersion().asc()
				// .count()
				// .listPage(firstResult, maxResults)
				// .singleResult()
				.list();// 总的结果集数量
		// 便利集合，查看内容
		for (ProcessDefinition pd : pdList) {
			System.out.println("id:" + pd.getId());
			System.out.println("name:" + pd.getName());
			System.out.println("key:" + pd.getKey());
			System.out.println("version:" + pd.getVersion());
			System.out.println("resourceName:" + pd.getDiagramResourceName());
			System.out.println("###########################################");
		}
	}

	@Test
	public void findGroupTaskList() {
		/*List<Task> dd = processEngine.getTaskService().createTaskQuery().processInstanceId("401").list();
		if (dd != null && dd.size() > 0) {
			for (Task task : dd) {
				System.out.println("任务ID：" + task.getId());
				System.out.println("任务的办理人：" + task.getAssignee());
				System.out.println("任务名称：" + task.getName());
				System.out.println("任务的创建时间：" + task.getCreateTime());
				System.out.println("流程实例ID：" + task.getProcessInstanceId());
				System.out.println("#######################################");
			}
		}
		List<Deployment> ddf =  processEngine.getRepositoryService().createDeploymentQuery().list();
		for (Deployment deployment : ddf) {
			System.out.println(deployment.getName());
		}*/
		
		
		// 任务办理人
		String candidateUser = "ganle";
		List<Task> list = processEngine.getTaskService()//
				.createTaskQuery()//
				.taskCandidateUser(candidateUser)// 参与者，组任务查询
				.list();
		if (list != null && list.size() > 0) {
			for (Task task : list) {
				System.out.println("任务ID：" + task.getId());
				System.out.println("任务的办理人：" + task.getAssignee());
				System.out.println("任务名称：" + task.getName());
				System.out.println("任务的创建时间：" + task.getCreateTime());
				System.out.println("流程实例ID：" + task.getProcessInstanceId());
				System.out.println("#######################################");
			}
		}
	}
	
	
	
}
