package ai.sparklabinc.woos.activiti.service;

import java.util.List;

import ai.sparklabinc.woos.commons.activiti.ActivitiCommand;
import ai.sparklabinc.woos.commons.activiti.ActivitiParamsBean;
import ai.sparklabinc.woos.commons.activiti.ActivitiResult;
import ai.sparklabinc.woos.entity.DataIdAndActivitiRelations;

/**
 * @title activiti服务
 * 
 *
 */
public interface ActivitiService {

	/**
	 * 批量启动工作流
	 * 
	 * @param dataIds
	 * @param modelIds
	 * @param actionCodes
	 * @throws Exception
	 */
	List<ActivitiResult> createBatch(List<ActivitiCommand> activitiCommands) throws Exception;

	/**
	 * 根据actionCode启动对应的工作流，并保存此工作流信息
	 * 
	 * @param dataId
	 * @param actionCode
	 *            unique
	 * @throws Exception
	 */
	// void create(String dataId, String modelId, String actionCode) throws
	// Exception;

	/**
	 * 审批工作流节点
	 * 
	 * @param dataId
	 * @param taskId
	 * @throws Exception
	 */
	List<ActivitiResult> approveProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception;

	/**
	 * 驳回 工作流
	 * 
	 * @param dataId
	 * @param moduleId
	 * @param taskId
	 * @throws Exception
	 */
	List<ActivitiResult> rejectProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception;

	/**
	 * 关闭 工作流
	 * 
	 * @param dataId
	 * @param moduleId
	 * @param taskId
	 * @throws Exception
	 */
	List<ActivitiResult> closeProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception;

	/**
	 * 查询对应数据当前的工作流状态及信息
	 * 
	 * @param dataId
	 * @param moduleId
	 * @return
	 * @throws Exception
	 */
	DataIdAndActivitiRelations query(String dataId, String modelId) throws Exception;

	/**
	 * 
	 * @param taskId
	 * @throws Exception
	 */
	void suspendProcess(String taskId) throws Exception;

	/**
	 * 
	 * @param taskId
	 * @throws Exception
	 */
	void activateProcess(String taskId) throws Exception;

	/**
	 * 将任务转发给指定人
	 * 
	 * @param processInstanceId
	 * @param userName
	 * @throws Exception
	 */
	boolean claimProcess(String processInstanceId, String fromUserName, String toUserName) throws Exception;
}
