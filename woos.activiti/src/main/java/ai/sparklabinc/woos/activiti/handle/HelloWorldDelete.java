package ai.sparklabinc.woos.activiti.handle;


import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.junit.Test;

public class HelloWorldDelete {
	
    //获取流程引擎对象  
    //getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息  
    ProcessEngine processEngine=ProcessEngines.getDefaultProcessEngine();  
    
	/**删除流程*/  
	@Test  
	public void deleteDeployment(){  
	    //删除发布信息  
	    String deploymentId="1";  
	    //获取仓库服务对象  
	    RepositoryService repositoryService=processEngine.getRepositoryService();  
	    //普通删除，如果当前规则下有正在执行的流程，则抛异常  
	    //repositoryService.deleteDeployment(deploymentId);  
	    //级联删除，会删除和当前规则相关的所有信息，正在执行的信息，也包括历史信息  
	    repositoryService.deleteDeployment(deploymentId, true);  
	      
	}  
}
