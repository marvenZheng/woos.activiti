package ai.sparklabinc.woos.activiti.handle;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.TaskService;
import org.junit.Test;
/**
 * 
* @Title: HelloWorldApprove.java 
* @Description: TODO(用一句话描述该文件做什么) 
* @author cx.zheng
* @date 2017-9-19 下午10:52:48 
*
 */
public class HelloWorldApprove {
	//获取流程引擎对象  
    //getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息  
    ProcessEngine processEngine=ProcessEngines.getDefaultProcessEngine();  
	
	
	 /**完成我的任务*/  
     @Test  
     public void completeMyPersonalTask(){  
         String taskId="3404";//上一次我们查询的任务ID就是304  
         TaskService taskService=processEngine.getTaskService(); 
         taskService.setAssignee("3404", "ceshi00015");
         taskService.complete(taskId);//完成taskId对应的任务  
         System.out.println("完成ID为"+taskId+"的任务");  
           
     }   
}
