package ai.sparklabinc.woos.activiti.service;

import java.net.InetAddress;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.TaskService;
import org.activiti.engine.impl.RepositoryServiceImpl;
import org.activiti.engine.impl.persistence.entity.ProcessDefinitionEntity;
import org.activiti.engine.impl.pvm.PvmTransition;
import org.activiti.engine.impl.pvm.process.ActivityImpl;
import org.activiti.engine.impl.pvm.process.TransitionImpl;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.IdentityLink;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ai.sparklabinc.woos.commons.ServiceException;
import ai.sparklabinc.woos.commons.activiti.ActivitiCommand;
import ai.sparklabinc.woos.commons.activiti.ActivitiParamsBean;
import ai.sparklabinc.woos.commons.activiti.ActivitiResult;
import ai.sparklabinc.woos.entity.Constants;
import ai.sparklabinc.woos.entity.DataIdAndActivitiRelations;
import ai.sparklabinc.woos.exception.ParamIsEmptyException;
import ai.sparklabinc.woos.repository.DataIdAndActivitiRelationsRepository;
import ai.sparklabinc.woos.util.JDBCUtil;
import ai.sparklabinc.woos.util.PropertiesUtil;

@Service
public class ActivitiServiceImpl implements ActivitiService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ActivitiServiceImpl.class);

	// 读取application.properties配置文件获取
	private PropertiesUtil propertiesUtil = new PropertiesUtil("application.properties");

	@Autowired
	private DataIdAndActivitiRelationsRepository dataIdAndActivitiRelationsRepository;

	// jdbc connect
	Connection conn = JDBCUtil.getConnection();

	// activiti engine
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	// @Override
	public ActivitiResult create(String dataId, String modelId, String actionCode, String estoreCode,
			String brandTypeIds) throws Exception {
		RuntimeService runtimeService = processEngine.getRuntimeService();
		ActivitiResult ar = new ActivitiResult();
		// 使用流程定义的key，key对应bpmn文件对应的id，
		// (也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动
		String processDefinitionkey = actionCode;// 流程定义的key就是HelloWorld
		// 获取流程实例对象
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionkey);
		// 如果actionCodes 是close_001则手动执行完所有的流程
		dealCloseCase(actionCode, processInstance);
		List<Deployment> dpDatas = processEngine.getRepositoryService().createDeploymentQuery().list();
		Map<String, String> datas = new HashMap<String, String>();
		for (Deployment deployment : dpDatas) {
			datas.put(deployment.getName(), deployment.getId());
		}
		System.out.println("流程实例ID：" + processInstance.getId());// 流程实例ID

		if (dataId.isEmpty() || actionCode.isEmpty())
			throw new ParamIsEmptyException(
					"param can not null, please check ." + (dataId.isEmpty() ? "dataId is null; " : "")
							+ (actionCode.isEmpty() ? " actionCode is null ;" : ""));
		// get usernames by estoreCode
		List<String> groupIds = new ArrayList<String>();
		ResultSet rs = conn.prepareStatement(
				"select group_id,username from id_membership where username in(select iub.username from id_user_business_property iub where iub.estore_codes like '%"
						+ estoreCode + "%' and iub.brand_ids like '%" + brandTypeIds + "%');")
				.executeQuery();
		while (rs.next()) {
			groupIds.add(rs.getString(1));
		}

		DataIdAndActivitiRelations data = new DataIdAndActivitiRelations();
		data.setDataId(dataId);
		data.setProcessInstaceId(processInstance.getId());
		data.setCurrentHandler("");
		data.setModelId(modelId);
		data.setActivitiPhotoPath("http://" + InetAddress.getLocalHost().getHostAddress() + ":"
				+ propertiesUtil.getValue("server.port") + "/diagrams/" + actionCode + ".png");
		data.setStatus(
				actionCode.equals(Constants.ACTIONCODES.Close_001.toString()) ? DataIdAndActivitiRelations.Status.CLOSED
						: DataIdAndActivitiRelations.Status.IN_PROCESS);
		this.dataIdAndActivitiRelationsRepository.save(data);
		ar.setDataId(dataId);
		//ar.setGroupName(Constants.actionCodeAndRoles.get(actionCode));
		ar.setModelId(modelId);
		ar.setStatus(actionCode.equals(Constants.ACTIONCODES.Close_001.toString())
				? DataIdAndActivitiRelations.Status.CLOSED.toString()
				: DataIdAndActivitiRelations.Status.IN_PROCESS.toString());
		ar.setProcessInstanceId(processInstance.getId());
		ar.setHandler(
				actionCode.equals(Constants.ACTIONCODES.Close_001.toString()) ? "system" : String.join(",", groupIds));
		return ar;
	}

	/**
	 * 处理actionCodes == Close_001的case
	 * 
	 * @param actionCode
	 * @param processInstance
	 */
	private void dealCloseCase(String actionCode, ProcessInstance processInstance) {
		if (actionCode.equals(Constants.ACTIONCODES.Close_001.toString())) {
			Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getId())
					.singleResult();
			processEngine.getTaskService().complete(task.getId());
		}
	}

	@Override
	public List<ActivitiResult> approveProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception {
		List<ActivitiResult> result = new ArrayList<ActivitiResult>();
		for (ActivitiParamsBean apb : activitiParamsBean) {
			ActivitiResult bean = new ActivitiResult();
			Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(apb.getProcessInstanceId())
					.singleResult();
			if (task == null) {
				LOGGER.error("can not get record ,the processInstaceId is :" + apb.getProcessInstanceId());
				break;
			}
			// 任务办理人
			TaskService taskService = processEngine.getTaskService();
			taskService.setAssignee(task.getId(), apb.getUsername());
			taskService.complete(task.getId());// 完成taskId对应的任务
			LOGGER.info("完成ID为" + task.getId() + "的任务");
			Task nextTask = processEngine.getTaskService().createTaskQuery()
					.processInstanceId(apb.getProcessInstanceId()).singleResult();
			List<IdentityLink> nextLinkUsers = new ArrayList<IdentityLink>();
			if (nextTask != null) {
				nextLinkUsers = processEngine.getTaskService().getIdentityLinksForTask(nextTask.getId());
			}
			List<String> users = nextLinkUsers.stream().map(data -> data.getUserId()).collect(Collectors.toList());
			bean.setHandler(nextLinkUsers.size() != 0 ? String.join(",", users) : apb.getUsername());
			bean.setStatus(nextLinkUsers.size() != 0 ? DataIdAndActivitiRelations.Status.IN_PROCESS.toString()
					: DataIdAndActivitiRelations.Status.COMPLETED.toString());
			bean.setProcessInstanceId(apb.getProcessInstanceId());
			result.add(bean);
		}
		return result;
	}

	@Override
	public List<ActivitiResult> rejectProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception {
		List<ActivitiResult> data = new ArrayList<ActivitiResult>();
		for (ActivitiParamsBean apb : activitiParamsBean) {
			data.add(this.rejectDetailProcess(apb));
		}
		return data;
	}

	private ActivitiResult rejectDetailProcess(ActivitiParamsBean activitiParamsBean) throws Exception {
		Task task = processEngine.getTaskService().createTaskQuery()
				.processInstanceId(activitiParamsBean.getProcessInstanceId()).singleResult();
		processEngine.getTaskService().complete(task.getId());
		LOGGER.info("reject process succeed");
		Task nextTask = processEngine.getTaskService().createTaskQuery()
				.processInstanceId(activitiParamsBean.getProcessInstanceId()).singleResult();
		if (nextTask != null) {
			rejectDetailProcess(activitiParamsBean);
		}
		ActivitiResult result = new ActivitiResult();
		result.setHandler(activitiParamsBean.getUsername());
		result.setStatus(DataIdAndActivitiRelations.Status.REJECTED.toString());
		result.setProcessInstanceId(activitiParamsBean.getProcessInstanceId());
		DataIdAndActivitiRelations entity = this.dataIdAndActivitiRelationsRepository
				.findByProcessInstanceId(activitiParamsBean.getProcessInstanceId());
		entity.setStatus(DataIdAndActivitiRelations.Status.REJECTED);
		entity.setCurrentHandler(activitiParamsBean.getUsername());
		this.dataIdAndActivitiRelationsRepository.save(entity);
		return result;
	}

	@Override
	public List<ActivitiResult> closeProcess(List<ActivitiParamsBean> activitiParamsBean) throws Exception {
		List<ActivitiResult> result = new ArrayList<ActivitiResult>();
		for (ActivitiParamsBean apb : activitiParamsBean) {
			result.add(this.closeDetailProcess(apb));
		}
		return result;
	}

	/**
	 * 关闭process
	 * 
	 * @param activitiParamsBean
	 * @return
	 * @throws Exception
	 */
	private ActivitiResult closeDetailProcess(ActivitiParamsBean activitiParamsBean) throws Exception {
		ActivitiResult bean = new ActivitiResult();
		Task task = processEngine.getTaskService().createTaskQuery()
				.processInstanceId(activitiParamsBean.getProcessInstanceId()).singleResult();
		if (task == null) {
			LOGGER.error("can not get record ,the processInstaceId is :" + activitiParamsBean.getProcessInstanceId());
			return null;
		}
		// 任务办理人
		TaskService taskService = processEngine.getTaskService();
		taskService.setAssignee(task.getId(), activitiParamsBean.getUsername());
		taskService.complete(task.getId());// 完成taskId对应的任务
		LOGGER.info("完成ID为" + task.getId() + "的任务");
		Task nextTask = processEngine.getTaskService().createTaskQuery()
				.processInstanceId(activitiParamsBean.getProcessInstanceId()).singleResult();
		if (nextTask != null) {
			this.closeDetailProcess(activitiParamsBean);
		}
		List<IdentityLink> nextLinkUsers = new ArrayList<IdentityLink>();
		if (nextTask != null) {
			nextLinkUsers = processEngine.getTaskService().getIdentityLinksForTask(nextTask.getId());
		}
		List<String> users = nextLinkUsers.stream().map(data -> data.getUserId()).collect(Collectors.toList());
		bean.setHandler(nextLinkUsers.size() != 0 ? String.join(",", users) : activitiParamsBean.getUsername());
		bean.setStatus(DataIdAndActivitiRelations.Status.CLOSED.toString());
		bean.setProcessInstanceId(activitiParamsBean.getProcessInstanceId());
		DataIdAndActivitiRelations entity = this.dataIdAndActivitiRelationsRepository
				.findByProcessInstanceId(activitiParamsBean.getProcessInstanceId());
		entity.setStatus(DataIdAndActivitiRelations.Status.CLOSED);
		entity.setCurrentHandler(activitiParamsBean.getUsername());
		this.dataIdAndActivitiRelationsRepository.save(entity);
		return bean;
	}

	@Override
	public DataIdAndActivitiRelations query(String dataId, String modelId) throws Exception {
		return this.dataIdAndActivitiRelationsRepository.findByDataId(dataId, modelId);
	}

	@Override
	public List<ActivitiResult> createBatch(List<ActivitiCommand> activitiCommands) throws Exception {
		List<ActivitiResult> data = new ArrayList<ActivitiResult>();
		for (ActivitiCommand ac : activitiCommands) {
			List<String> aclists = Arrays.asList(ac.getActionCodes().split(","));
			for (int i = 0; i < aclists.size(); i++) {
				data.add(this.create(ac.getDataId(), ac.getModelId(), aclists.get(i), ac.getEstoreCode(),
						ac.getBrands()));
			}
		}
		return data;
	}

	@Override
	public void suspendProcess(String taskId) throws Exception {
		Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
		processEngine.getRuntimeService().suspendProcessInstanceById(task.getProcessInstanceId());
	}

	@Override
	public void activateProcess(String taskId) throws Exception {
		Task task = processEngine.getTaskService().createTaskQuery().taskId(taskId).singleResult();
		processEngine.getRuntimeService().activateProcessInstanceById(task.getProcessInstanceId());
	}

	@Override
	public boolean claimProcess(String processInstanceId, String fromUserName, String toUserName) throws Exception {
		Task task = processEngine.getTaskService().createTaskQuery().processInstanceId(processInstanceId)
				.singleResult();
		String taskId = task.getId();
		List<IdentityLink> linkUsers = processEngine.getTaskService().getIdentityLinksForTask(task.getId());
		List<String> users = linkUsers.stream().map(data -> data.getUserId()).collect(Collectors.toList());
		if (!users.contains(fromUserName)) {
			LOGGER.error("该用户：" + fromUserName + " 不在执行名单中。请联系管理员！");
			return false;
		}
		try {
			processEngine.getTaskService().setAssignee(taskId, toUserName);
			// processEngine.getTaskService().claim(taskId, toUserName);
			// claim only can set one time , Do not adapt our business
		} catch (Exception e) {
			LOGGER.error(e.getMessage());
			return false;
		}
		return true;
	}

	@Test
	public void test() throws ServiceException {
		this.rejectTask("601", "startevent1", "this is my reject message");
	}

	/**
	 * 驳回工作流到指定节点，
	 * 
	 * @param procInstanId
	 *            实例id
	 * @param destTaskKey
	 *            驳回到的指定节点
	 * @param rejectMessage
	 *            驳回信息
	 * @throws ServiceException
	 */
	public void rejectTask(String procInstanId, String destTaskKey, String rejectMessage) throws ServiceException {
		// 获得当前任务的对应实列
		TaskService taskService = processEngine.getTaskService();
		Task taskEntity = taskService.createTaskQuery().processInstanceId(procInstanId).singleResult();
		// 当前任务key
		String taskDefKey = taskEntity.getTaskDefinitionKey();
		// 获得当前流程的定义模型
		RepositoryService repositoryService = processEngine.getRepositoryService();
		ProcessDefinitionEntity processDefinition = (ProcessDefinitionEntity) ((RepositoryServiceImpl) repositoryService)
				.getDeployedProcessDefinition(taskEntity.getProcessDefinitionId());
		// 获得当前流程定义模型的所有任务节点
		List<ActivityImpl> activitilist = processDefinition.getActivities();
		// 获得当前活动节点和驳回的目标节点"draft"
		ActivityImpl currActiviti = null;// 当前活动节点
		ActivityImpl destActiviti = null;// 驳回目标节点
		int sign = 0;
		for (ActivityImpl activityImpl : activitilist) {
			// 确定当前活动activiti节点
			if (taskDefKey.equals(activityImpl.getId())) {
				currActiviti = activityImpl;
				sign++;
			} else if (destTaskKey.equals(activityImpl.getId())) {
				destActiviti = activityImpl;
				sign++;
			}
			// System.out.println("//-->activityImpl.getId():"+activityImpl.getId());
			if (sign == 2) {
				break;// 如果两个节点都获得,退出跳出循环
			}
		}
		System.out.println("//-->currActiviti activityImpl.getId():" + currActiviti.getId());
		System.out.println("//-->destActiviti activityImpl.getId():" + destActiviti.getId());
		// 保存当前活动节点的流程想参数
		List<PvmTransition> hisPvmTransitionList = new ArrayList<PvmTransition>(0);
		for (PvmTransition pvmTransition : currActiviti.getOutgoingTransitions()) {
			hisPvmTransitionList.add(pvmTransition);
		}
		// 清空当前活动几点的所有流出项
		currActiviti.getOutgoingTransitions().clear();
		System.out.println(
				"//-->currActiviti.getOutgoingTransitions().clear():" + currActiviti.getOutgoingTransitions().size());
		// 为当前节点动态创建新的流出项
		TransitionImpl newTransitionImpl = currActiviti.createOutgoingTransition();
		// 为当前活动节点新的流出目标指定流程目标
		newTransitionImpl.setDestination(destActiviti);
		// 保存驳回意见
		taskEntity.setDescription(rejectMessage);// 设置驳回意见
		taskService.saveTask(taskEntity);
		// 设定驳回标志
		// 执行当前任务驳回到目标任务draft
		taskService.complete(taskEntity.getId());
		// 清除目标节点的新流入项
		destActiviti.getIncomingTransitions().remove(newTransitionImpl);
		// 清除原活动节点的临时流程项
		currActiviti.getOutgoingTransitions().clear();
		// 还原原活动节点流出项参数
		currActiviti.getOutgoingTransitions().addAll(hisPvmTransitionList);
	}

}
