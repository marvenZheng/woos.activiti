package ai.sparklabinc.woos.activiti.handle;

import java.util.Date;
import java.util.List;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RuntimeService;
import org.activiti.engine.runtime.ProcessInstance;
import org.activiti.engine.task.Task;
import org.junit.Test;
import org.springframework.beans.factory.InitializingBean;

//@Component
public class HelloWorldStart implements InitializingBean {

	// 获取流程引擎对象
	// getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	/** 启动流程引擎 */
	// @Test
	public void startProcessInstance() {
		// 获取流程启动Service
		RuntimeService runtimeService = processEngine.getRuntimeService();
		// 使用流程定义的key，key对应bpmn文件对应的id，
		// (也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动
		String processDefinitionkey = "HelloWorld";// 流程定义的key就是HelloWorld
		// 获取流程实例对象
		// runtimeService.startProcessInstanceByKey(processDefinitionkey, variables)
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionkey);
		System.out.println("流程实例ID：" + processInstance.getId());// 流程实例ID
		System.out.println("流程定义ID：" + processInstance.getProcessDefinitionId());// 流程定义ID
		System.out.println(processInstance.getProcessInstanceId());

	}

	@Test
	public void startPO_002ProcessInstance() {
		Date date1 = new Date();

			// 获取流程启动Service
			RuntimeService runtimeService = processEngine.getRuntimeService();
			// 使用流程定义的key，key对应bpmn文件对应的id，
			// (也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动
			String processDefinitionkey = "PO_001";// 流程定义的key就是HelloWorld
			// runtimeService.set
			// 获取流程实例对象
			// Map<String, Object> variables = new HashMap<String, Object>();
			// variables.put("username", "marven,max,danny,sharon");
			// ProcessInstance processInstance =
			// runtimeService.startProcessInstanceByKey(processDefinitionkey, variables);
			ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionkey);
			// List<Task> tt =
			// processEngine.getTaskService().createTaskQuery().processInstanceId(processInstance.getProcessInstanceId()).list();

			// System.out.println("流程实例ID：" + processInstance.getId());// 流程实例ID
			// System.out.println("流程定义ID：" + processInstance.getProcessDefinitionId());//
			// 流程定义ID
		Date date2 = new Date();

		System.out.println(date2.getTime() - date1.getTime());

	}

	@Override
	public void afterPropertiesSet() throws Exception {
		/*
		 * // 获取流程启动Service RuntimeService runtimeService =
		 * processEngine.getRuntimeService(); // 使用流程定义的key，key对应bpmn文件对应的id， //
		 * (也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动 String processDefinitionkey =
		 * "helloWorld";// 流程定义的key就是HelloWorld // 获取流程实例对象 ProcessInstance
		 * processInstance =
		 * runtimeService.startProcessInstanceByKey(processDefinitionkey);
		 * System.out.println("流程实例ID：" + processInstance.getId());// 流程实例ID
		 * System.out.println("流程定义ID：" + processInstance.getProcessDefinitionId());//
		 * 流程定义ID
		 */
		// 获取流程启动Service
		RuntimeService runtimeService = processEngine.getRuntimeService();
		// 使用流程定义的key，key对应bpmn文件对应的id，
		// (也是act_re_procdef表中对应的KEY_字段),默认是按照最新版本启动
		String processDefinitionkey = "MD_001";// 流程定义的key就是HelloWorld
		// runtimeService.set
		// 获取流程实例对象
		// Map<String, Object> variables = new HashMap<String, Object>();
		// variables.put("username", "marven,max,danny,sharon");
		// ProcessInstance processInstance =
		// runtimeService.startProcessInstanceByKey(processDefinitionkey, variables);
		ProcessInstance processInstance = runtimeService.startProcessInstanceByKey(processDefinitionkey);
		System.out.println("流程实例ID：" + processInstance.getId());// 流程实例ID
		System.out.println("流程定义ID：" + processInstance.getProcessDefinitionId());// 流程定义ID
	}

}