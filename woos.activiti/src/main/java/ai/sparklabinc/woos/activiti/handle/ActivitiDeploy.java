package ai.sparklabinc.woos.activiti.handle;

import java.util.List;
import java.util.stream.Collectors;

import org.activiti.engine.ProcessEngine;
import org.activiti.engine.ProcessEngines;
import org.activiti.engine.RepositoryService;
import org.activiti.engine.repository.Deployment;
import org.activiti.engine.repository.DeploymentBuilder;
import org.junit.Test;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

/**
 * init all workflow Process
 * 
 * @author marven
 *
 */
@Component
public class ActivitiDeploy implements InitializingBean {

	// 获取流程引擎对象
	// getDefaultProcessEngine方法内部会自动读取名为activiti.cfg.xml文件的配置信息
	ProcessEngine processEngine = ProcessEngines.getDefaultProcessEngine();

	@Override
	public void afterPropertiesSet() throws Exception {
		// 获取已经发布的所有工作流流程
		List<Deployment> deployments = processEngine.getRepositoryService().createDeploymentQuery().list();
		List<String> deployedNames = deployments.stream().map(data -> data.getName()).collect(Collectors.toList());
		// 与流程定义和部署对象相关的Service
		RepositoryService repositoryService = processEngine.getRepositoryService();

		// init MD_001 workflow process
		if (!deployedNames.contains("MD_001")) {
			DeploymentBuilder MD_001Builder = repositoryService.createDeployment();// 创建一个部署对象
			MD_001Builder.name("MD_001");// 添加部署的名称
			MD_001Builder.addClasspathResource("static/diagrams/MD_001.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			MD_001Builder.addClasspathResource("static/diagrams/MD_001.png");// 从classpath的资源加载，一次只能加载一个文件
			MD_001Builder.deploy();// 完成部署
		}
		// init MD_002 workflow process
		if (!deployedNames.contains("MD_002")) {
			DeploymentBuilder MD_002Builder = repositoryService.createDeployment();
			MD_002Builder.name("MD_002");
			MD_002Builder.addClasspathResource("static/diagrams/MD_002.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			MD_002Builder.addClasspathResource("static/diagrams/MD_002.png");// 从classpath的资源加载，一次只能加载一个文件
			MD_002Builder.deploy();// 完成部署
		}
		// init PO_001 workflow process
		if (!deployedNames.contains("PO_001")) {
			DeploymentBuilder PO_001Builder = repositoryService.createDeployment();
			PO_001Builder.name("PO_001");
			PO_001Builder.addClasspathResource("static/diagrams/PO_001.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			PO_001Builder.addClasspathResource("static/diagrams/PO_001.png");// 从classpath的资源加载，一次只能加载一个文件
			PO_001Builder.deploy();// 完成部署
		}
		// init PO_002 workflow process
		if (!deployedNames.contains("PO_002")) {
			DeploymentBuilder PO_002Builder = repositoryService.createDeployment();
			PO_002Builder.name("PO_002");
			PO_002Builder.addClasspathResource("static/diagrams/PO_002.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			PO_002Builder.addClasspathResource("static/diagrams/PO_002.png");// 从classpath的资源加载，一次只能加载一个文件
			PO_002Builder.deploy();// 完成部署
		}
		// init Close_001 workflow process
		if (!deployedNames.contains("Close_001")) {
			DeploymentBuilder Close_001Builder = repositoryService.createDeployment();
			Close_001Builder.name("Close_001");
			Close_001Builder.addClasspathResource("static/diagrams/Close_001.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			Close_001Builder.addClasspathResource("static/diagrams/Close_001.png");// 从classpath的资源加载，一次只能加载一个文件
			Close_001Builder.deploy();// 完成部署
		}
		// init PS_001 workflow process
		if (!deployedNames.contains("PS_001")) {
			DeploymentBuilder PS_001Builder = repositoryService.createDeployment();
			PS_001Builder.name("PS_001");
			PS_001Builder.addClasspathResource("static/diagrams/PS_001.bpmn");// 从classpath的资源加载，一次只能加载一个文件
			PS_001Builder.addClasspathResource("static/diagrams/PS_001.png");// 从classpath的资源加载，一次只能加载一个文件
			PS_001Builder.deploy();// 完成部署
		}
	}

	@Test
	public void deploymentPO_002() {
		// 与流程定义和部署对象相关的Service
		RepositoryService repositoryService = processEngine.getRepositoryService();

		DeploymentBuilder deploymentBuilder = repositoryService.createDeployment();// 创建一个部署对象
		deploymentBuilder.name("PO_001");// 添加部署的名称
		deploymentBuilder.addClasspathResource("diagrams/PO_001.bpmn");// 从classpath的资源加载，一次只能加载一个文件
		deploymentBuilder.addClasspathResource("diagrams/PO_001.png");// 从classpath的资源加载，一次只能加载一个文件

		Deployment deployment = deploymentBuilder.deploy();// 完成部署
		System.out.println(deployment.getId());
		/** 添加用户角色组 */
		/*
		 * IdentityService identityService = processEngine.getIdentityService();// //
		 * 创建角色 identityService.saveGroup(new GroupEntity("总经理"));
		 * identityService.saveGroup(new GroupEntity("部门经理")); // 创建用户
		 * identityService.saveUser(new UserEntity("张三")); identityService.saveUser(new
		 * UserEntity("李四")); identityService.saveUser(new UserEntity("王五")); //
		 * 建立用户和角色的关联关系 identityService.createMembership("张三", "部门经理");
		 * identityService.createMembership("李四", "部门经理");
		 * identityService.createMembership("王五", "总经理");
		 * System.out.println("添加组织机构成功"); //打印我们的流程信息
		 * System.out.println("流程Id:"+deployment.getId());
		 * System.out.println("流程Name:"+deployment.getName());
		 */
	}
}
