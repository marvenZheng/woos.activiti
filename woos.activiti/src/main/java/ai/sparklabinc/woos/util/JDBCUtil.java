package ai.sparklabinc.woos.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCUtil {
    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    //获得连接
    public static Connection getConnection(){
        Connection conn = null;
        String url = "jdbc:mysql://127.0.0.1:3306/identity?useUnicode=true&amp;characterset=utf-8&amp;autoReconnect=true&useSSL=false";
        String user = "root";
        String password = "root";
        try {
            conn = DriverManager.getConnection(url, user, password);
            //System.out.println("connection is successful!");
        } catch (SQLException e) {
            System.out.println("connection error！");
            e.printStackTrace();
        }
        return conn;
    }
    //释放JDBC资源（关闭顺序与声明时的顺序相反）
    public static void release(Connection conn,Statement state,ResultSet rs){
        if(rs != null){
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(state != null){
            try {
                state.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if(conn != null){
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
